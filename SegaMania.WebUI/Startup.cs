﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SegaMania.WebUI.Startup))]
namespace SegaMania.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
