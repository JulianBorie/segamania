﻿using SegaMania.Data;
using SegaMania.Logic;
using SegaMania.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SegaMania.WebUI.Controllers
{
    public class JeuController : Controller
    {
        public ActionResult ToutLesJeux()
        {
            List<ConsoleSega> listConsoles = RequestManager.ToutLesJeuxParConsole();
            return View(listConsoles);
        }

        public ActionResult DetailsJeu(string nomSeo)
        {
            var vm = RequestManager.DetailsJeu(nomSeo);
            return View(vm);
        }
    }
}