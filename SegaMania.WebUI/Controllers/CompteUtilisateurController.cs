﻿using Microsoft.AspNet.Identity;
using SegaMania.Data;
using SegaMania.Logic;
using SegaMania.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SegaMania.WebUI.Controllers
{
    public class CompteUtilisateurController : Controller
    {
        string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();

        public ActionResult MesAvis()
        {
                return View();
        }

        public ActionResult MesAvisConsolePartial()
        {
            var vm = RequestManager.MesAvisConsole(userId);
            bool isEmpty = !vm.Any();
            ViewBag.isEmpty = isEmpty;
            if (isEmpty)
            {
                TempData["noItemConsole"] = "noItem";
                return PartialView("MesAvisConsolePartial");
            }
            return PartialView("MesAvisConsolePartial", vm);
        }

        public ActionResult MesAvisJeuPartial()
        {
            var vm = RequestManager.MesAvisJeu(userId);
            bool isEmpty = !vm.Any();
            ViewBag.isEmpty = isEmpty;
            if (isEmpty)
            {
                TempData["noItemJeu"] = "noItem";
                return PartialView("MesAvisJeuPartial");
            }
            return PartialView("MesAvisJeuPartial", vm);
        }

        public ActionResult MesJeux()
        {
            var vm = RequestManager.MesJeux(userId);
            ViewBag.MaListe = vm;
            return View(vm);
        }

        public ActionResult MesConsoles()
        {
            var vm = RequestManager.MesConsoles(userId);
            return View(vm);
        }

        public ActionResult SupprimerConsole(int id)
        {
            RequestManager.Suppression(id, userId, "ConsoleSega");
            return RedirectToAction("MesConsoles", "CompteUtilisateur");
        }

        public ActionResult SupprimerJeu(int id)
        {
            RequestManager.Suppression(id, userId, "Jeu");
            return RedirectToAction("MesJeux", "CompteUtilisateur");
        }
    }
}