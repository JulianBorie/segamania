﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SegaMania.Data;
using SegaMania.Logic;
using SegaMania.WebUI.Models;

namespace SegaMania.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult test()
        {
            return View();
        }

        public ActionResult Accueil()
        {
            var vm = RequestManager.Accueil();
            return View(vm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult MoteurDeRecherchePartial()
        {
            var vm = new RechercheViewModel();
            return PartialView("_MoteurDeRecherchePartial", vm);
        }

        //[HttpPost]
        //public ActionResult MoteurDeRecherchePartial(RechercheViewModel recherche)
        //{
        //    using (var context = new SegaManiaDbEntities())
        //    {
        //        var listConsoleSega = context.ConsoleSega.OrderBy(x => Guid.NewGuid()).Take(3).ToList();
        //        List<string> listeResultats = context.SP_LISTE_JEUX_PAR_CONSOLE(consoleEntity.Id).ToList();


        //         = context.ConsoleSega.FirstOrDefault(f => f.NomSeo == avis.nomSeo);
        //        if (consoleEntity == null)
        //        {
        //            return RedirectToAction("Accueil", "Home");
        //        }

        //        AvisConsole nouvelAvis = new AvisConsole();
        //        nouvelAvis.DateAvis = DateTime.Now;
        //        nouvelAvis.Description = avis.commentaire;


        //        var userId = User.Identity.GetUserId();
        //        nouvelAvis.UserId = userId;

        //        var mgerUnicite = new UniqueAvisVerification();
        //        if (!mgerUnicite.AutorisationDeCommenterConsole(userId, consoleEntity.Id))
        //        {
        //            TempData["Message"] = "Désolé très cher internaute mais un avis pour cette console a déjà était posté.";
        //            return RedirectToAction("DetailsConsole", "ConsoleSega", new { nomSeo = avis.nomSeo });
        //        }

        //        var mger = new PersonneManager();
        //        nouvelAvis.Nom = mger.GetNomFromUserId(userId);

        //        Double dNote = 0;
        //        if (!Double.TryParse(avis.note, NumberStyles.Any, CultureInfo.InvariantCulture, out dNote))
        //        {
        //            throw new Exception("Impossible de parser la note" + avis.note);
        //        }
        //        nouvelAvis.Note = dNote;

        //        nouvelAvis.IdConsole = consoleEntity.Id;

        //        context.AvisConsole.Add(nouvelAvis);
        //        try
        //        {
        //            context.SaveChanges();
        //        }
        //        catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
        //        {
        //            Exception raise = dbEx;
        //            foreach (var validationErrors in dbEx.EntityValidationErrors)
        //            {
        //                foreach (var validationError in validationErrors.ValidationErrors)
        //                {
        //                    string message = string.Format("{0}:{1}",
        //                        validationErrors.Entry.Entity.ToString(),
        //                        validationError.ErrorMessage);
        //                    // raise a new exception nesting
        //                    // the current instance as InnerException
        //                    raise = new InvalidOperationException(message, raise);
        //                }
        //            }
        //            throw raise;
        //        }
        //    }
        //    return RedirectToAction("DetailsConsole", "ConsoleSega", new { nomSeo = avis.nomSeo });
        //}
    }
}