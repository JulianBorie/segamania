﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SegaMania.Data;
using SegaMania.WebUI.Models;
using SegaMania.Logic;

namespace SegaMania.WebUI.Controllers
{
    public class ConsoleSegaController : Controller
    {
        public ActionResult ToutesLesConsoles()
        {
            List<ConsoleSega> listConsoles = RequestManager.ListConsoles();
            return View(listConsoles);
        }

        public ActionResult DetailsConsole(string nomSeo)
        {
            var vm = RequestManager.DetailsConsole(nomSeo);
            return View(vm);
        }
    }
}