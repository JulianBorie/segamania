﻿using SegaMania.Data;
using SegaMania.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SegaMania.WebUI.Controllers
{
    public class CollectionController : Controller
    {
        public ActionResult AjoutCollection(int id,string userId, string table)
        {
            if (table == "jeuAssoPersonne")
            {
                RequestManager.AjoutCollection(id, userId, "jeuAssoPersonne");
                return RedirectToAction("ToutLesJeux", "Jeu");
            }
                RequestManager.AjoutCollection(id, userId, "consoleAssoPersonne");
                return RedirectToAction("ToutesLesConsoles", "ConsoleSega");
        }
    }
}