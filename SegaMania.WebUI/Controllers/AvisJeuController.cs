﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using SegaMania.Data;
using SegaMania.Logic;
using SegaMania.WebUI.Models;

namespace SegaMania.WebUI.Controllers
{
    public class AvisJeuController : Controller
    {
        [Authorize]
        public ActionResult LaisserUnAvis(string nomSeo)
        {
            var vm = RequestManager.LaisserUnAvis(nomSeo, "AvisJeu");
            if (vm == null)
            {
                return RedirectToAction("Accueil", "Home");
            }
            return View("../Avis/LaisserUnAvis", vm);
        }

        [Authorize]
        [HttpPost]
        public ActionResult SaveComment(AvisViewModel avis)
        {
            var userId = User.Identity.GetUserId();
            var vm = RequestManager.SaveComment(avis, userId, "AvisJeu");
            if (!vm)
            {
                TempData["Message"] = "Désolé très cher internaute mais un avis pour ce jeu a déjà était posté.";
                return RedirectToAction("DetailsJeu", "Jeu", new { nomSeo = avis.nomSeo });
            }
            return RedirectToAction("DetailsJeu", "Jeu", new { nomSeo = avis.nomSeo });
        }

        public ActionResult SupprimerAvis(int id)
        {
            var userId = User.Identity.GetUserId();
            var vm = RequestManager.SupprimerAvis(id, userId, "AvisJeu");
            if (!vm)
            {
                TempData["MessageAvis"] = "N'essayez pas de supprimer l'avis de quelqu'un d'autre, c'est mal !";
                return RedirectToAction("MesAvis", "CompteUtilisateur");
            }
            return RedirectToAction("MesAvis", "CompteUtilisateur");
        }

        public ActionResult listAvis(string nomSeo)
        {
            if (nomSeo != null)
            {
                var vm = RequestManager.listAvisJeu(nomSeo);
                if ((vm != null) && (vm.Any()))
                {
                    ViewBag.JeuOuConsole = "Jeu";
                    ViewBag.ListAvis = vm;
                    return View("../Avis/listAvis");
                }
            }
            return RedirectToAction("Accueil", "Home");
        }
    }
}