﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SegaMania.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "contact",
                url: "Contact",
                defaults: new { controller = "Contact", action = "Index" }
            );

            routes.MapRoute(
                name: "detailsConsole",
                url: "ConsoleSega/{nomSeo}",
                defaults: new { controller = "ConsoleSega", action = "DetailsConsole" }
            );

            routes.MapRoute(
                name: "touteslesconsolessega",
                url: "toutes-les-consoles-Sega",
                defaults: new { controller = "ConsoleSega", action = "ToutesLesConsoles" }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Accueil", id = UrlParameter.Optional }
            );
        }
    }
}
