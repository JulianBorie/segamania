﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegaMania.Data
{
    public class ConsoleSegaAvecAvisDto
    {
        public ConsoleSega ConsoleSega { get; set; }
        public double Note { get; set; }
    }
}
