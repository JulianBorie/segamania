﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SegaMania.Data;

namespace SegaMania.Logic
{
    public static class UniqueAvisVerification
    {
        public static bool AutorisationDeCommenterConsole(string userId, int consoleId)
        {
            using (var context = new SegaManiaDbEntities())
            {
                var personEntity = context.AvisConsole.FirstOrDefault(a => a.UserId == userId && a.IdConsole == consoleId);
                if (personEntity == null)
                {
                    return true;
                }
                return false;
            }
        }

        public static bool AutorisationDeCommenterJeu(string userId, int jeuId)
        {
            using (var context = new SegaManiaDbEntities())
            {
                var personEntity = context.AvisJeu.FirstOrDefault(a => a.UserId == userId && a.IdJeu == jeuId);
                if (personEntity == null)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
