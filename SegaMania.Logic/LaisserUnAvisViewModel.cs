﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SegaMania.Logic
{
    public class LaisserUnAvisViewModel
    {
        public string Name { get; set; }
        public string NomSeo { get; set; }
        public string Controller { get; set; }

    }
}