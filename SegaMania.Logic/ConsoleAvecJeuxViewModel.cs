﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SegaMania.Data;

namespace SegaMania.Logic
{
    public class ConsoleAvecJeuxViewModel
    {
        public string ConsoleNom { get; internal set; }
        public string ConsoleNomSeo { get; internal set; }
        public string ConsoleDescription { get; internal set; }
        public DateTime ConsoleDateDeSortie { get; internal set; }
        public List<Jeu> Jeux { get; internal set; }
        public List<AvisConsole> AvisConsole { get; internal set; }
        public double Note { get; set; }
        public int NombreAvisConsole { get; set; }
    }
}