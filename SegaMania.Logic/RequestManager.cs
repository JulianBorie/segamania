﻿using SegaMania.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Globalization;
using System.Web.Mvc;
using System.Security.Principal;
using System.Data.Entity;

namespace SegaMania.Logic
{
    public static class RequestManager
    {
        /////////////////////////////////////////////////////////////////////////////////////////// AvisConsoleController
        public static LaisserUnAvisViewModel LaisserUnAvis(string nomSeo, string controller)
        {
            var vm = new LaisserUnAvisViewModel();
            vm.NomSeo = nomSeo;
            using (var context = new SegaManiaDbEntities())
            {
                if (controller == "AvisConsole")
                {
                    vm.Controller = "AvisConsole";
                    var entityConsole = context.ConsoleSega.FirstOrDefault(f => f.NomSeo == nomSeo);
                    if (entityConsole == null)
                    {
                        vm = null;
                        return vm;
                    }
                    vm.Name = entityConsole.Nom;
                    return vm;
                }
                var entityJeu = context.Jeu.FirstOrDefault(f => f.NomSeo == nomSeo);
                if (entityJeu == null)
                {
                    vm = null;
                    return vm;
                }
                vm.Name = entityJeu.Nom;
                return vm;
            }
        }

        public static bool SaveComment(AvisViewModel avis, string userId, string controller)
        {
            bool mgerUnicite;
            using (var context = new SegaManiaDbEntities())
            {
                if (controller == "AvisConsole")
                {
                    var consoleEntity = context.ConsoleSega.FirstOrDefault(f => f.NomSeo == avis.nomSeo); //je veux la consoleSega dont le nom seo est égale à la valeur en paramètre

                    AvisConsole nouvelAvisConsole = new AvisConsole();
                    nouvelAvisConsole.DateAvis = DateTime.Now;
                    nouvelAvisConsole.Description = avis.commentaire;

                    nouvelAvisConsole.UserId = userId;

                    mgerUnicite = UniqueAvisVerification.AutorisationDeCommenterConsole(userId, consoleEntity.Id);
                    if (mgerUnicite == false)
                    {
                        return false;
                    }

                    var mgerConsole = new PersonneManager();
                    nouvelAvisConsole.Nom = mgerConsole.GetNomFromUserId(userId);

                    Double dNoteConsole = 0;
                    if (!Double.TryParse(avis.note, NumberStyles.Any, CultureInfo.InvariantCulture, out dNoteConsole))
                    {
                        throw new Exception("Impossible de parser la note" + avis.note);
                    }
                    nouvelAvisConsole.Note = dNoteConsole;

                    nouvelAvisConsole.IdConsole = consoleEntity.Id;

                    context.AvisConsole.Add(nouvelAvisConsole);
                }
                else
                {
                    var jeuEntity = context.Jeu.FirstOrDefault(f => f.NomSeo == avis.nomSeo);

                    AvisJeu nouvelAvisJeu = new AvisJeu();
                    nouvelAvisJeu.DateAvis = DateTime.Now;
                    nouvelAvisJeu.Description = avis.commentaire;

                    nouvelAvisJeu.UserId = userId;

                    mgerUnicite = UniqueAvisVerification.AutorisationDeCommenterJeu(userId, jeuEntity.Id);
                    if (mgerUnicite == false)
                    {
                        return false;
                    }

                    var mgerJeu = new PersonneManager();
                    nouvelAvisJeu.Nom = mgerJeu.GetNomFromUserId(userId);

                    Double dNoteJeu = 0;
                    if (!Double.TryParse(avis.note, NumberStyles.Any, CultureInfo.InvariantCulture, out dNoteJeu))
                    {
                        throw new Exception("Impossible de parser la note" + avis.note);
                    }
                    nouvelAvisJeu.Note = dNoteJeu;

                    nouvelAvisJeu.IdJeu = jeuEntity.Id;

                    context.AvisJeu.Add(nouvelAvisJeu);
                }
                try
                {
                    context.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            }
            return true;
        }

        public static bool SupprimerAvis(int id, string userId, string controller)
        {
            bool saveOrnot = false;
            using (var context = new SegaManiaDbEntities())
            {
                //var avisEntity = context.DYNAMIC_TABLE_NAME(controller).FirstOrDefault(f => f.Id == id && f.UserId == userId);
                if (controller == "AvisConsole")
                {
                    AvisConsole avisEntityConsole = context.AvisConsole.FirstOrDefault(f => f.Id == id && f.UserId == userId);
                    if (avisEntityConsole != null)
                    {
                        context.AvisConsole.Remove(avisEntityConsole);
                        context.SaveChanges();
                        return saveOrnot = true;
                    }
                }
                var avisEntityJeu = context.AvisJeu.FirstOrDefault(f => f.Id == id && f.UserId == userId);
                if (avisEntityJeu != null)
                {
                    context.AvisJeu.Remove(avisEntityJeu);
                    context.SaveChanges();
                    return saveOrnot = true;
                }
            }
            return saveOrnot;
        }

        public static List<AvisConsole> listAvisConsole(string nomSeo)
        {
            List<AvisConsole> listAvis;
            using (var context = new SegaManiaDbEntities())
            {
                listAvis = context.AvisConsole.Where(c => c.ConsoleSega.NomSeo == nomSeo).OrderByDescending(c => c.DateAvis).ToList();
            }
            return listAvis;
        }

        public static List<AvisJeu> listAvisJeu(string nomSeo)
        {
            List<AvisJeu> listAvis;
            using (var context = new SegaManiaDbEntities())
            {
                listAvis = context.AvisJeu.Where(c => c.Jeu.NomSeo == nomSeo).OrderByDescending(c => c.DateAvis).ToList();
            }
            return listAvis;
        }

        /////////////////////////////////////////////////////////////////////////////////////////// CollectionController
        public static void AjoutCollection(int id, string userId, string table)
        {
            using (var context = new SegaManiaDbEntities())
            {
                if (table == "jeuAssoPersonne")
                {
                    JeuAssoPersonne jeuAssoPersonne = context.JeuAssoPersonne.Where(r => r.JeuId == id && r.UserId == userId).FirstOrDefault();
                    if (jeuAssoPersonne == null)
                    {
                        jeuAssoPersonne = new JeuAssoPersonne();
                        jeuAssoPersonne.JeuId = id;
                        jeuAssoPersonne.UserId = userId;
                        context.JeuAssoPersonne.Add(jeuAssoPersonne);
                        context.SaveChanges();
                    }
                }
                else
                {
                    ConsoleAssoPersonne consoleAssoPersonne = context.ConsoleAssoPersonne.Where(r => r.ConsoleId == id && r.UserId == userId).FirstOrDefault();
                    if (consoleAssoPersonne == null)
                    {
                        consoleAssoPersonne = new ConsoleAssoPersonne();
                        consoleAssoPersonne.ConsoleId = id;
                        consoleAssoPersonne.UserId = userId;
                        context.ConsoleAssoPersonne.Add(consoleAssoPersonne);
                        context.SaveChanges();
                    }
                }
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////// CompteUtilisateurController
        public static List<AvisViewModel> MesAvisConsole(string userId)
        {
            List<AvisViewModel> listAvisConsole = new List<AvisViewModel>();
            using (var context = new SegaManiaDbEntities())
            {
                List<AvisConsole> listAvis = context.AvisConsole.Where(c => c.UserId == userId).OrderByDescending(c => c.DateAvis).ToList();
                foreach (AvisConsole a in listAvis)
                {
                    var vm = new AvisViewModel();
                    vm.nom = a.ConsoleSega.Nom;
                    vm.commentaire = a.Description;
                    vm.DateAvis = a.DateAvis;
                    vm.noteDouble = a.Note;
                    vm.id = a.Id;
                    listAvisConsole.Add(vm);
                }
                return listAvisConsole;
            }
        }

        public static List<AvisViewModel> MesAvisJeu(string userId)
        {
            List<AvisViewModel> listAvisJeu = new List<AvisViewModel>();
            using (var context = new SegaManiaDbEntities())
            {
                List<AvisJeu> listAvis = context.AvisJeu.Where(c => c.UserId == userId).OrderByDescending(c => c.DateAvis).ToList();
                foreach (AvisJeu a in listAvis)
                {
                    var vm = new AvisViewModel();
                    vm.nom = a.Jeu.Nom;
                    vm.commentaire = a.Description;
                    vm.DateAvis = a.DateAvis;
                    vm.noteDouble = a.Note;
                    vm.id = a.Id;
                    listAvisJeu.Add(vm);
                }
                return listAvisJeu;
            }
        }

        public static List<Jeu> MesJeux(string userId)
        {
            using (var context = new SegaManiaDbEntities())
            {
                List<Jeu> listeJeux = context.SP_LISTE_JEUX_PAR_UTILISATEUR(userId).ToList();
                return listeJeux;
            }
        }

        public static List<test_Result> MesConsoles(string userId)
        {
            using (var context = new SegaManiaDbEntities())
            {
                List<test_Result> listeConsoles = context.SP_LISTE_CONSOLES_PAR_UTILISATEUR(userId).ToList();
                return listeConsoles;
            }
        }

        public static void Suppression(int id, string userId, string controller)
        {
            using (SegaManiaDbEntities context = new SegaManiaDbEntities())
            {
                if (controller == "ConsoleSega")
                {
                    ConsoleAssoPersonne consoleEntity = context.ConsoleAssoPersonne.FirstOrDefault(c => c.ConsoleId == id && c.UserId == userId);
                    if (consoleEntity != null)
                    {
                        context.ConsoleAssoPersonne.Remove(consoleEntity);
                        context.SaveChanges();
                    }
                }
                else
                {
                    JeuAssoPersonne jeuEntity = context.JeuAssoPersonne.FirstOrDefault(c => c.JeuId == id && c.UserId == userId);
                    if (jeuEntity != null)
                    {
                        context.JeuAssoPersonne.Remove(jeuEntity);
                        context.SaveChanges();
                    }
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////// ConsoleSegaController
        public static List<ConsoleSega> ListConsoles()
        {
            List<ConsoleSega> list = new List<ConsoleSega>();
            using (var context = new SegaManiaDbEntities())
            {
                list = context.ConsoleSega.ToList();
            }
            return list;
        }

        public static ConsoleAvecJeuxViewModel DetailsConsole(string nomSeo)
        {
            var vm = new ConsoleAvecJeuxViewModel();
            using (var context = new SegaManiaDbEntities())
            {
                var consoleEntity = context.ConsoleSega.Where(f => f.NomSeo == nomSeo).FirstOrDefault();
                if (consoleEntity != null)
                {
                    vm.ConsoleNom = consoleEntity.Nom;
                    vm.ConsoleNomSeo = consoleEntity.NomSeo;
                    vm.ConsoleDescription = consoleEntity.Description;
                    vm.ConsoleDateDeSortie = consoleEntity.DateDeSortie;
                    if (consoleEntity.AvisConsole.Count > 0)
                    {
                        vm.Note = consoleEntity.AvisConsole.Average(a => a.Note);
                    }
                    vm.NombreAvisConsole = consoleEntity.AvisConsole.Count;
                    vm.AvisConsole = consoleEntity.AvisConsole.ToList();
                    vm.Jeux = consoleEntity.Jeu.ToList();
                    List<Jeu> listeJeux = context.SP_LISTE_JEUX_PAR_CONSOLE(consoleEntity.Id).ToList();
                }
                else
                {
                    vm = null;
                }
            }
            return vm;
        }

        /////////////////////////////////////////////////////////////////////////////////////////// HomeController
        public static AccueilViewModel Accueil()
        {
            var vm = new AccueilViewModel();
            using (var context = new SegaManiaDbEntities())
            {
                var listConsoleSega = context.ConsoleSega.OrderBy(x => Guid.NewGuid()).Take(3).ToList();

                foreach (var f in listConsoleSega)
                {
                    var dto = new ConsoleSegaAvecAvisDto();
                    dto.ConsoleSega = f;
                    if (f.AvisConsole.Count == 0)
                    {
                        dto.Note = 0;
                    }
                    else
                    {
                        dto.Note = Math.Round(f.AvisConsole.Average(a => a.Note), 2);
                    }
                    vm.ListConsolesSega.Add(dto);
                }
            }
            return vm;
        }

        /////////////////////////////////////////////////////////////////////////////////////////// JeuController
        public static List<ConsoleSega> ToutLesJeuxParConsole()
        {
            using (var context = new SegaManiaDbEntities())
            {
                var listeConsoles = context.ConsoleSega.Include(console => console.Jeu).Include(console => console.AvisConsole).ToList();
                return listeConsoles;
            }
        }

        public static JeuViewModel DetailsJeu(string nomSeo)
        {
            var vm = new JeuViewModel();
            using (var context = new SegaManiaDbEntities())
            {
                var jeuEntity = context.Jeu.Where(f => f.NomSeo == nomSeo).FirstOrDefault();
                var consoleEntity = context.ConsoleSega.Where(c => c.Id == jeuEntity.ConsoleSegaId).FirstOrDefault();
                if (jeuEntity != null && consoleEntity != null)
                {
                    vm.JeuNom = jeuEntity.Nom;
                    vm.JeuNomSeo = jeuEntity.NomSeo;
                    vm.JeuDescription = jeuEntity.Description;
                    vm.JeuDateDeSortie = jeuEntity.DateDeSortie;
                    if (jeuEntity.AvisJeu.Count > 0)
                    {
                        vm.Note = jeuEntity.AvisJeu.Average(a => a.Note);
                    }
                    vm.NombreAvisJeu = jeuEntity.AvisJeu.Count;
                    vm.AvisJeu = jeuEntity.AvisJeu.ToList();
                    vm.NomConsole = consoleEntity.Nom;
                }
                else
                {
                    vm = null;
                }
            }
            return vm;
        }
    }
}