﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SegaMania.Logic
{
    public class AvisViewModel
    {
        public string commentaire { get; set; }
        public System.DateTime DateAvis { get; set; }
        public string nom { get; set; }
        public string note { get; set; }
        public double noteDouble { get; set; }
        public string nomSeo { get; set; }
        public int id { get; set; }

    }
}