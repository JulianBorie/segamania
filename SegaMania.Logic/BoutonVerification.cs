﻿using SegaMania.Data;
using System.Linq;

namespace SegaMania.Logic
{
    public class BoutonVerification
    {
        public static bool BoutonAjoutJeuCollection(string userId, int jeuId)
        {
            using (var context = new SegaManiaDbEntities())
            {
                var collectionEntity = context.JeuAssoPersonne.FirstOrDefault(c => c.UserId == userId && c.JeuId == jeuId);
                if (collectionEntity == null)
                {
                    return true;
                }
                return false;
            }
        }

        public static bool BoutonAjoutConsoleCollection(string userId, int consoleId)
        {
            using (var context = new SegaManiaDbEntities())
            {
                var collectionEntity = context.ConsoleAssoPersonne.FirstOrDefault(c => c.UserId == userId && c.ConsoleId == consoleId);
                if (collectionEntity == null)
                {
                    return true;
                }
                return false;
            }
        }
    }
}