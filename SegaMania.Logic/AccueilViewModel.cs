﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SegaMania.Data;

namespace SegaMania.Logic
{
    public class AccueilViewModel
    {
        public AccueilViewModel()
        {
            ListConsolesSega = new List<ConsoleSegaAvecAvisDto>();
        }

        public List<ConsoleSegaAvecAvisDto> ListConsolesSega { get; set; }
    }
}