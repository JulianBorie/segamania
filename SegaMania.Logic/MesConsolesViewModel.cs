﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegaMania.Logic
{
    public class MesConsolesViewModel
    {
        public string Nom { get; internal set; }
        public int Id { get; internal set; }
        public string NomSeo { get; internal set; }
    }
}
