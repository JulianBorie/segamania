﻿using SegaMania.Data;
using System;
using System.Collections.Generic;


namespace SegaMania.Logic
{
    public class JeuViewModel
    {
        public string JeuNom { get; internal set; }
        public string JeuNomSeo { get; internal set; }
        public string JeuDescription { get; internal set; }
        public DateTime JeuDateDeSortie { get; internal set; }
        public List<AvisJeu> AvisJeu { get; internal set; }
        public double Note { get; set; }
        public int NombreAvisJeu { get; set; }
        public string NomConsole { get; internal set; }

    }
}