﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Web;
using Moq;
using SegaMania.WebUI;

namespace SegaMania.Tests
{
    [TestClass]
    public class RouteTests
    {
        private static RouteData DefinirUrl(string url)
        {
            Mock<HttpContextBase> mockContext = new Mock<HttpContextBase>();
            mockContext.Setup(c => c.Request.AppRelativeCurrentExecutionFilePath).Returns(url);
            RouteCollection routes = new RouteCollection();
            RouteConfig.RegisterRoutes(routes);
            RouteData routeData = routes.GetRouteData(mockContext.Object);
            return routeData;
        }

        [TestMethod]
        public void Routes_PageHome_RetourneControleurHomeEtMethodeAccueil()
        {
            RouteData routeData = DefinirUrl("~/");
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Home", routeData.Values["controller"]);
            Assert.AreEqual("Accueil", routeData.Values["action"]);
        }

        [TestMethod]
        public void Routes_DetailsConsole_RetourneControleurConsoleSegaEtMethodeDetailsConsole()
        {
            RouteData routeData = DefinirUrl("~/ConsoleSega/Sega-Saturn");
            Assert.IsNotNull(routeData);
            Assert.AreEqual("ConsoleSega", routeData.Values["controller"]);
            Assert.AreEqual("DetailsConsole", routeData.Values["action"]);
            Assert.AreEqual("Sega-Saturn", routeData.Values["nomSeo"]);
        }

        [TestMethod]
        public void Routes_Contact_RetourneControleurContactEtMethodeIndex()
        {
            RouteData routeData = DefinirUrl("~/Contact");
            Assert.IsNotNull(routeData);
            Assert.AreEqual("Contact", routeData.Values["controller"]);
            Assert.AreEqual("Index", routeData.Values["action"]);
        }

        [TestMethod]
        public void Routes_ToutesLesConsolesSega_RetourneControleurConsoleSegaEtMethodeToutesLesConsoles()
        {
            RouteData routeData = DefinirUrl("~/toutes-les-consoles-Sega");
            Assert.IsNotNull(routeData);
            Assert.AreEqual("ConsoleSega", routeData.Values["controller"]);
            Assert.AreEqual("ToutesLesConsoles", routeData.Values["action"]);
        }
    }
}
