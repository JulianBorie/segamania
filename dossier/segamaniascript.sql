Create SegaManiaDb
GO

CREATE TABLE dbo.__MigrationHistory
	(
	MigrationId    NVARCHAR (150) NOT NULL,
	ContextKey     NVARCHAR (300) NOT NULL,
	Model          VARBINARY (max) NOT NULL,
	ProductVersion NVARCHAR (32) NOT NULL,
	CONSTRAINT PK_dbo.__MigrationHistory PRIMARY KEY (MigrationId, ContextKey)
	)
GO

CREATE TABLE dbo.AspNetRoles
	(
	Id   NVARCHAR (128) NOT NULL,
	Name NVARCHAR (256) NOT NULL,
	CONSTRAINT PK_dbo.AspNetRoles PRIMARY KEY (Id)
	)
GO

CREATE UNIQUE INDEX RoleNameIndex
	ON dbo.AspNetRoles (Name)
GO

CREATE TABLE dbo.AspNetUserClaims
	(
	Id         INT IDENTITY NOT NULL,
	UserId     NVARCHAR (128) NOT NULL,
	ClaimType  NVARCHAR (max) NULL,
	ClaimValue NVARCHAR (max) NULL,
	CONSTRAINT PK_dbo.AspNetUserClaims PRIMARY KEY (Id),
	CONSTRAINT FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId FOREIGN KEY (UserId) REFERENCES dbo.AspNetUsers (Id) ON DELETE CASCADE
	)
GO

CREATE INDEX IX_UserId
	ON dbo.AspNetUserClaims (UserId)
GO

CREATE TABLE dbo.AspNetUserLogins
	(
	LoginProvider NVARCHAR (128) NOT NULL,
	ProviderKey   NVARCHAR (128) NOT NULL,
	UserId        NVARCHAR (128) NOT NULL,
	CONSTRAINT PK_dbo.AspNetUserLogins PRIMARY KEY (LoginProvider, ProviderKey, UserId),
	CONSTRAINT FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId FOREIGN KEY (UserId) REFERENCES dbo.AspNetUsers (Id) ON DELETE CASCADE
	)


CREATE INDEX IX_UserId
	ON dbo.AspNetUserLogins (UserId)
GO

CREATE TABLE dbo.AspNetUserRoles
	(
	UserId NVARCHAR (128) NOT NULL,
	RoleId NVARCHAR (128) NOT NULL,
	CONSTRAINT PK_dbo.AspNetUserRoles PRIMARY KEY (UserId, RoleId),
	CONSTRAINT FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId FOREIGN KEY (RoleId) REFERENCES dbo.AspNetRoles (Id) ON DELETE CASCADE,
	CONSTRAINT FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId FOREIGN KEY (UserId) REFERENCES dbo.AspNetUsers (Id) ON DELETE CASCADE
	)
GO

CREATE INDEX IX_UserId
	ON dbo.AspNetUserRoles (UserId)
GO

CREATE INDEX IX_RoleId
	ON dbo.AspNetUserRoles (RoleId)
GO

CREATE TABLE dbo.AspNetUsers
	(
	Id                   NVARCHAR (128) NOT NULL,
	Email                NVARCHAR (256) NULL,
	EmailConfirmed       BIT NOT NULL,
	PasswordHash         NVARCHAR (max) NULL,
	SecurityStamp        NVARCHAR (max) NULL,
	PhoneNumber          NVARCHAR (max) NULL,
	PhoneNumberConfirmed BIT NOT NULL,
	TwoFactorEnabled     BIT NOT NULL,
	LockoutEndDateUtc    DATETIME NULL,
	LockoutEnabled       BIT NOT NULL,
	AccessFailedCount    INT NOT NULL,
	UserName             NVARCHAR (256) NOT NULL,
	CONSTRAINT PK_dbo.AspNetUsers PRIMARY KEY (Id)
	)
GO

CREATE UNIQUE INDEX UserNameIndex
	ON dbo.AspNetUsers (UserName)
GO

CREATE TABLE dbo.AvisConsole
	(
	Id          INT IDENTITY NOT NULL,
	Nom         VARCHAR (100) NOT NULL,
	Description VARCHAR (500) NULL,
	Note        FLOAT NOT NULL,
	IdConsole   INT NOT NULL,
	DateAvis    DATETIME2 NOT NULL,
	UserId      NVARCHAR (128) NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK__AvisConsole__IdConsole__15502E78 FOREIGN KEY (IdConsole) REFERENCES dbo.ConsoleSega (Id)
	)
GO

CREATE TABLE AvisJeu
	(
	Id          INT IDENTITY NOT NULL,
	Nom         VARCHAR (100) NOT NULL,
	Description VARCHAR (500) NULL,
	Note        FLOAT NOT NULL,
	IdJeu       INT NOT NULL,
	DateAvis    DATETIME2 NOT NULL,
	UserId      NVARCHAR (128) NOT NULL,
	CONSTRAINT PK_AvisJeu PRIMARY KEY (Id),
	CONSTRAINT FK_AvisJeu_Jeu FOREIGN KEY (IdJeu) REFERENCES dbo.Jeu (Id)
	)
GO

CREATE TABLE ConsoleAssoPersonne
	(
	Id        INT IDENTITY NOT NULL,
	UserId    NVARCHAR (128) NOT NULL,
	ConsoleId INT NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK__ConsoleAssoPersonne__ConsoleId__15502E78 FOREIGN KEY (ConsoleId) REFERENCES dbo.ConsoleSega (Id),
	CONSTRAINT FK__ConsoleAssoPersonne__UserId__15502E78 FOREIGN KEY (UserId) REFERENCES dbo.Personne (UserId)
	);

Create SegaManiaDb

CREATE TABLE ConsoleSega
	(
	Id           INT IDENTITY NOT NULL,
	Nom          VARCHAR (100) NOT NULL,
	Description  VARCHAR (1000) NOT NULL,
	DateDeSortie DATE NOT NULL,
	NomSeo       VARCHAR (100) NULL,
	PRIMARY KEY (Id)
	);

CREATE TABLE Personne
	(
	UserId           INT IDENTITY NOT NULL,
	Nom          NVARCHAR (100) NOT NULL,
	PRIMARY KEY (UserId)
	);

CREATE TABLE ConsoleAssoPersonne
	(
	Id        INT IDENTITY NOT NULL,
	UserId    NVARCHAR (128) NOT NULL,
	ConsoleId INT NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK__ConsoleAssoPersonne__ConsoleId__15502E78 FOREIGN KEY (ConsoleId) REFERENCES dbo.ConsoleSega (Id),
	CONSTRAINT FK__ConsoleAssoPersonne__UserId__15502E78 FOREIGN KEY (UserId) REFERENCES dbo.Personne (UserId)
	);


CREATE procedure [dbo].[SP_LISTE_CONSOLES_PAR_UTILISATEUR]
(
@UserId nvarchar(128)
)
AS
select * from ConsoleSega
inner join ConsoleAssoPersonne on ConsoleSega.Id = ConsoleAssoPersonne.ConsoleId
inner join Personne on ConsoleAssoPersonne.UserId = Personne.UserId
where Personne.UserId = @UserId
GO

CREATE procedure [dbo].[SP_LISTE_JEUX_PAR_CONSOLE]
(
@ConsoleSegaId int
)
AS
SELECT *
FROM [dbo].[Jeu]
Where [dbo].[Jeu].[ConsoleSegaId]=@ConsoleSegaId

GO

CREATE procedure [dbo].[SP_LISTE_JEUX_PAR_UTILISATEUR]
(
@UserId nvarchar(128)
)
AS
select * from Jeu
inner join JeuAssoPersonne on Jeu.Id = JeuAssoPersonne.JeuId
inner join Personne on JeuAssoPersonne.UserId = Personne.UserId
where Personne.UserId = @UserId
GO

